from django.shortcuts import render
from django.http import HttpResponse
from .models import Client, Truckreport, Clientreport, Clienttype, Reportresolution, Gender, Search, Usersession, Foodpreference, Clientreporthistory, AuthUser, Language
from django.db.models import Q
import datetime
from django.http import JsonResponse
from django.contrib.auth.decorators import login_required

@login_required
def index(request, **kwargs):
	clients = Client.objects

	language = 0
	clientname = ''
	gender = 0
	clientstatus = ''
	fromdate = ''
	todate = ''

	if request.GET.get('clientname'):
		clientname = request.GET['clientname']
		if clientname is None:
			clientname = ''
		clients = clients.filter(Q(name__icontains=request.GET['clientname']) | Q(lastname__icontains=request.GET['clientname']))

	if request.GET.get('gender'):
		gender = int(request.GET['gender'])
		clients = clients.filter(id_gender=gender)

	if request.GET.get('language'):
		language = int(request.GET['language'])
		clients = clients.filter(id_language=language)

	if request.GET.get('clientstatus'):
		clientstatus = request.GET.get('clientstatus')
		clients = clients.filter(active=bool(int(request.GET.get('clientstatus'))))

	if request.GET.get('fromdate'):
		fromdate = request.GET.get('fromdate')
		clients = clients.filter(created_at__gte=fromdate)

	if request.GET.get('todate'):
		todate = request.GET.get('todate')
		clients = clients.filter(created_at__lte=todate)

	clients = clients.order_by('name')[:100]
	languages = Language.objects.order_by('language').all
	genders = Gender.objects.order_by('gender').all

	context = { 
				'clients': clients, 
				'languages': languages, 
				'genders': genders, 
				'clientname': clientname, 
				'language': language, 
				'gender': gender, 
				'clientstatus': clientstatus, 
				'todate': todate, 
				'fromdate': fromdate 
			}
	return render(request, 'client/index.html', context)

@login_required
def detail(request, id):
	fromdate = ''
	todate = ''

	client = Client.objects.get(id = id)
	truckreports = Truckreport.objects.filter(id_client=id)
	clientreports = Clientreport.objects.filter(id_client_reported=id)
	search = Search.objects.filter(id_usersession__id_client=id)
	sessions = Usersession.objects.filter(id_client=id)
	cusinepreferences = Foodpreference.objects.filter(id_client=id).order_by('created_at')

	if request.GET.get('fromdate'):
		fromdate = request.GET.get('fromdate')
		clientreports = clientreports.filter(created_at__gte=fromdate)
		truckreports = truckreports.filter(created_at__gte=fromdate)
		search = search.filter(created_at__gte=fromdate)
		sessions = sessions.filter(created_at__gte=fromdate)

	if request.GET.get('todate'):
		todate = request.GET.get('todate')
		clientreports = clientreports.filter(created_at__lte=todate)
		truckreports = truckreports.filter(created_at__lte=todate)
		search = search.filter(created_at__lte=todate)
		sessions = sessions.filter(created_at__lte=todate)

	truckreports = truckreports.order_by('created_at')[:100]
	clientreports = clientreports.order_by('created_at')[:100]
	search = search.order_by('created_at')[:100]
	sessions = sessions.order_by('created_at')[:100]

	reportresolutions = Reportresolution.objects.order_by('resolution').all
	context = {'client': client, 'truckreports': truckreports, 'clientreports': clientreports, 'search': search, 'sessions': sessions, 'reportresolutions': reportresolutions, 'cusinepreferences': cusinepreferences, 'fromdate': fromdate, 'todate': todate }
	return render(request, 'client/detail.html', context) 

@login_required
def blockManagement(request):
	id = int(request.POST.get('id'))
	active = int(request.POST.get('active'))
	Client.objects.filter(id = id).update(active = active)
	return JsonResponse({'success': True})

@login_required
def clientComplaintResolution(request):
	id = int(request.POST.get('id'))
	idResolution = int(request.POST.get('resolution'))
	Clientreport.objects.filter(id = id).update(id_resolutiontype = idResolution)

	crh = Clientreporthistory()
	crh.id_clientreport = Clientreport(id=id)
	crh.id_reportresolution = Reportresolution(id=idResolution)
	crh.id_auth_user = AuthUser(id=request.user.id)
	crh.created_at = datetime.datetime.now()
	crh.save()
	return JsonResponse({'success': True})
