# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from __future__ import unicode_literals

from django.db import models


class AuthGroup(models.Model):
    name = models.CharField(unique=True, max_length=80)

    class Meta:
        managed = False
        db_table = 'auth_group'


class AuthGroupPermissions(models.Model):
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)
    permission = models.ForeignKey('AuthPermission', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_group_permissions'
        unique_together = (('group', 'permission'),)


class AuthPermission(models.Model):
    name = models.CharField(max_length=255)
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING)
    codename = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'auth_permission'
        unique_together = (('content_type', 'codename'),)


class AuthUser(models.Model):
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField(blank=True, null=True)
    is_superuser = models.BooleanField()
    username = models.CharField(unique=True, max_length=150)
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    email = models.CharField(max_length=254)
    is_staff = models.BooleanField()
    is_active = models.BooleanField()
    date_joined = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'auth_user'


class AuthUserGroups(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_groups'
        unique_together = (('user', 'group'),)


class AuthUserUserPermissions(models.Model):
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    permission = models.ForeignKey(AuthPermission, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_user_permissions'
        unique_together = (('user', 'permission'),)


class Client(models.Model):
    name = models.TextField()
    lastname = models.TextField()
    picture = models.TextField(blank=True, null=True)
    active = models.BooleanField()
    created_at = models.DateTimeField()
    registeredlanguage = models.TextField()
    id_language = models.ForeignKey('Language', models.DO_NOTHING, db_column='id_language')
    email = models.TextField()
    phone = models.TextField(blank=True, null=True)
    id_gender = models.ForeignKey('Gender', models.DO_NOTHING, db_column='id_gender')
    birthdate = models.TextField(blank=True, null=True)
    facebookid = models.BigIntegerField(blank=True, null=True)
    active = models.BooleanField()

    class Meta:
        managed = False
        db_table = 'client'


class Clientreport(models.Model):
    id_client_truck = models.ForeignKey(Client, models.DO_NOTHING, db_column='id_client_truck', related_name='truck_id_client_truck')
    id_client_reported = models.ForeignKey(Client, models.DO_NOTHING, db_column='id_client_reported', related_name='client_id_client_truck')
    active = models.BooleanField()
    created_at = models.DateTimeField()
    valid = models.NullBooleanField()
    id_reporttype = models.ForeignKey('Reporttype', models.DO_NOTHING, db_column='id_reporttype')
    id_truck = models.ForeignKey('Truck', models.DO_NOTHING, db_column='id_truck')
    id_resolutiontype = models.ForeignKey('Reportresolution', models.DO_NOTHING, db_column='id_resolutiontype', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'clientreport'


class Clientreporthistory(models.Model):
    id_clientreport = models.ForeignKey(Clientreport, models.DO_NOTHING, db_column='id_clientreport')
    id_reportresolution = models.ForeignKey('Reportresolution', models.DO_NOTHING, db_column='id_reportresolution')
    created_at = models.DateTimeField()
    id_auth_user = models.ForeignKey(AuthUser, models.DO_NOTHING, db_column='id_auth_user')

    class Meta:
        managed = False
        db_table = 'clientreporthistory'


class Clienttype(models.Model):
    type = models.TextField()
    active = models.BooleanField()

    class Meta:
        managed = False
        db_table = 'clienttype'


class Cusinetype(models.Model):
    trucktype_en = models.TextField()
    active = models.BooleanField()
    trucktype_es = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'cusinetype'


class Device(models.Model):
    device = models.TextField()

    class Meta:
        managed = False
        db_table = 'device'


class DjangoAdminLog(models.Model):
    action_time = models.DateTimeField()
    object_id = models.TextField(blank=True, null=True)
    object_repr = models.CharField(max_length=200)
    action_flag = models.SmallIntegerField()
    change_message = models.TextField()
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING, blank=True, null=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'django_admin_log'


class DjangoContentType(models.Model):
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'django_content_type'
        unique_together = (('app_label', 'model'),)


class DjangoMigrations(models.Model):
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class DjangoSession(models.Model):
    session_key = models.CharField(primary_key=True, max_length=40)
    session_data = models.TextField()
    expire_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_session'


class DjangoSite(models.Model):
    domain = models.CharField(unique=True, max_length=100)
    name = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'django_site'


class Foodpreference(models.Model):
    id_client = models.ForeignKey(Client, models.DO_NOTHING, db_column='id_client')
    id_cusinetype = models.ForeignKey(Cusinetype, models.DO_NOTHING, db_column='id_cusinetype')
    created_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'foodpreference'


class Foodtype(models.Model):
    foodtype_en = models.TextField()
    active = models.BooleanField()
    foodtype_es = models.TextField()

    class Meta:
        managed = False
        db_table = 'foodtype'


class Gender(models.Model):
    gender = models.TextField()
    active = models.BooleanField()

    class Meta:
        managed = False
        db_table = 'gender'


class Language(models.Model):
    language = models.TextField()

    class Meta:
        managed = False
        db_table = 'language'


class Menu(models.Model):
    title_en = models.TextField()
    description_en = models.TextField()
    price = models.FloatField()
    specialprice = models.FloatField(blank=True, null=True)
    active = models.BooleanField()
    id_client = models.ForeignKey('Client', models.DO_NOTHING, db_column='id_client')
    id_foodtype = models.ForeignKey(Foodtype, models.DO_NOTHING, db_column='id_foodtype')
    created_at = models.DateTimeField()
    description_es = models.TextField()
    picture = models.TextField()
    title_es = models.TextField()
    id_truck = models.ForeignKey('Truck', models.DO_NOTHING, db_column='id_truck')

    class Meta:
        managed = False
        db_table = 'menu'


class Reportresolution(models.Model):
    id = models.IntegerField(primary_key=True)
    resolution = models.TextField()
    active = models.BooleanField()

    class Meta:
        managed = False
        db_table = 'reportresolution'


class Reporttype(models.Model):
    reporttype = models.TextField()
    #active = models.BooleanField()
    forclient = models.BooleanField()
    fortruck = models.BooleanField()

    class Meta:
        managed = False
        db_table = 'reporttype'


class Search(models.Model):
    id = models.BigAutoField(primary_key=True)
    id_usersession = models.ForeignKey('Usersession', models.DO_NOTHING, db_column='id_usersession')
    searchedtext = models.TextField()
    created_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'search'


class Truck(models.Model):
    truckname = models.TextField()
    active = models.BooleanField()
    truckimage = models.TextField(blank=True, null=True)
    trucklogo = models.TextField(blank=True, null=True)
    description_en = models.TextField(blank=True, null=True)
    id_cusinetype = models.ForeignKey(Cusinetype, models.DO_NOTHING, db_column='id_cusinetype')
    allday = models.BooleanField()
    businessemail = models.TextField(blank=True, null=True)
    businessaddress = models.TextField(blank=True, null=True)
    phone = models.TextField(blank=True, null=True)
    confirmed = models.BooleanField()
    created_at = models.DateTimeField()
    description_es = models.TextField()

    class Meta:
        managed = False
        db_table = 'truck'


class Truckfeedback(models.Model):
    id_client = models.ForeignKey(Client, models.DO_NOTHING, db_column='id_client')
    id_truck = models.IntegerField()
    created_at = models.DateTimeField()
    feedback = models.TextField()
    active = models.BooleanField()
    score = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'truckfeedback'


class Truckmember(models.Model):
    id_client = models.ForeignKey(Client, models.DO_NOTHING, db_column='id_client')
    id_truck = models.ForeignKey(Truck, models.DO_NOTHING, db_column='id_truck')
    active = models.BooleanField()
    created_at = models.DateTimeField()
    deleted = models.BooleanField()
    id_clienttype = models.ForeignKey('Clienttype', models.DO_NOTHING, db_column='id_clienttype')

    class Meta:
        managed = False
        db_table = 'truckmember'


class Truckreport(models.Model):
    id_truck = models.ForeignKey(Truck, models.DO_NOTHING, db_column='id_truck')
    id_client = models.ForeignKey(Client, models.DO_NOTHING, db_column='id_client')
    created_at = models.DateTimeField()
    active = models.BooleanField()
    valid = models.BooleanField()
    id_reporttype = models.ForeignKey(Reporttype, models.DO_NOTHING, db_column='id_reporttype', blank=True, null=True)
    id_resolutiontype = models.ForeignKey(Reportresolution, models.DO_NOTHING, db_column='id_resolutiontype', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'truckreport'


class Truckreporthistory(models.Model):
    id_truckreport = models.ForeignKey(Truckreport, models.DO_NOTHING, db_column='id_truckreport')
    id_reportresolution = models.ForeignKey(Reportresolution, models.DO_NOTHING, db_column='id_reportresolution')
    id_auth_user = models.ForeignKey(AuthUser, models.DO_NOTHING, db_column='id_auth_user')
    created_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'truckreporthistory'


class Trucksession(models.Model):
    id_truck = models.ForeignKey(Truck, models.DO_NOTHING, db_column='id_truck')
    id_usersession = models.ForeignKey('Usersession', models.DO_NOTHING, db_column='id_usersession')
    latitude = models.FloatField(blank=True, null=True)
    longitude = models.FloatField(blank=True, null=True)
    created_at = models.DateTimeField()
    opens_at = models.DateTimeField()
    close_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'trucksession'


class Usersession(models.Model):
    id_client = models.IntegerField()
    latitude = models.FloatField(blank=True, null=True)
    longitude = models.FloatField(blank=True, null=True)
    active = models.BooleanField()
    id_device = models.ForeignKey(Device, models.DO_NOTHING, db_column='id_device')
    uuid = models.TextField(blank=True, null=True)
    sessionguid = models.TextField()
    created_at = models.DateTimeField(blank=True, null=True)
    fbtoken = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'usersession'
