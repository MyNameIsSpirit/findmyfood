function blockManagement(id, active, url){
	var csrftoken = getCookie('csrftoken');
	$.post(url, { id: id, active: active }, function(result){
       	window.location.reload();
    });
}

function truckResolveComplaint(id, resolution, url){
	$.post(url, { id: id, resolution: resolution }, function(result){
		$.growl({ title: "Find my food", message: "The truck complaint was updated." });
	});
}

function clientResolveComplaint(id, resolution, url){
	$.post(url, { id: id, resolution: resolution }, function(result){
		$.growl({ title: "Find my food", message: "The client complaint was updated." });
	});
}

$(function(){
	$("#clientprofile").dialog({
		autoOpen: false,
		width: "600px", 
		modal: true
	});

	$("#turckprofile").dialog({
		autoOpen: false,
		width: "600px", 
		modal: true
	});
});