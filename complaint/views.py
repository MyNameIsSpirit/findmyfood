from django.shortcuts import render
from django.http import HttpResponse
from client.models import Clientreport, Truckreport, Reporttype, Reportresolution, Client
from django.db.models import Q
from django.db.models import Max, F
from django.http import JsonResponse
from django.contrib.auth.decorators import login_required

@login_required
def clients(request):
	clientreports = Clientreport.objects
	complainttype = 0
	complaintstatus = 1
	clientname = ''
	truckname = ''
	fromdate = ''
	todate = ''

	if request.GET.get('complainttype'):
		complainttype = int(request.GET['complainttype'])
		clientreports = clientreports.filter(id_reporttype=complainttype)

	if request.GET.get('complaintstatus'):
		complaintstatus = int(request.GET['complaintstatus'])
		if complaintstatus == 1:
			clientreports = clientreports.filter(id_resolutiontype__isnull=True)
	else:
		clientreports = clientreports.filter(id_resolutiontype__isnull=True)

	if request.GET.get('clientname'):
		clientname = request.GET.get('clientname')
		if clientname is None:
			clientname = ''
		clientreports = clientreports.filter(Q(id_client_reported__name__icontains=clientname) | Q(id_client_reported__lastname__icontains=clientname) | Q(id_client_truck__name__icontains=clientname) | Q(id_client_truck__lastname__icontains=clientname))

	if request.GET.get('truckname'):
		truckname = request.GET.get('truckname')
		if truckname is None:
			truckname = ''
		clientreports = clientreports.filter(Q(id_truck__truckname__icontains=truckname) | Q(id_truck__truckname__icontains=truckname))

	if request.GET.get('fromdate'):
		fromdate = request.GET.get('fromdate')
		clientreports = clientreports.filter(created_at__gte=fromdate)

	if request.GET.get('todate'):
		todate = request.GET.get('todate')
		clientreports = clientreports.filter(created_at__lte=todate)

	clientreports = clientreports.order_by('created_at')[:100]

	reporttypes = Reporttype.objects.filter(forclient = True).order_by('reporttype').all()
	reportresolutions = Reportresolution.objects.order_by('resolution')
	context = { 'clientreports': clientreports, 'reporttypes': reporttypes, 'reportresolutions': reportresolutions , 'clientname': clientname, 'complainttype': complainttype, 'complaintstatus': complaintstatus, 'truckname': truckname, 'fromdate': fromdate, 'todate': todate }
	return render(request, 'complaint/clients.html', context)

@login_required
def trucks(request):
	truckreports = Truckreport.objects

	complainttype = 0
	complaintstatus = 1
	clientname = ''
	truckname = ''
	fromdate = ''
	todate = ''

	if request.GET.get('complainttype'):
		complainttype = int(request.GET['complainttype'])
		truckreports = truckreports.filter(id_reporttype=complainttype)

	if request.GET.get('complaintstatus'):
		complaintstatus = int(request.GET['complaintstatus'])
		if complaintstatus == 1:
			truckreports = truckreports.filter(id_resolutiontype__isnull=True)
	else:
		truckreports = truckreports.filter(id_resolutiontype__isnull=True)

	if request.GET.get('clientname'):
		clientname = request.GET.get('clientname')
		if clientname is None:
			clientname = ''
		truckreports = truckreports.filter(Q(id_client__name__icontains=clientname) | Q(id_client_lastname__icontains=clientname))

	if request.GET.get('truckname'):
		truckname = request.GET.get('truckname')
		if truckname is None:
			truckname = ''
		truckreports = truckreports.filter(Q(id_truck__truckname__icontains=truckname) | Q(id_truck__truckname__icontains=truckname))

	if request.GET.get('fromdate'):
		fromdate = request.GET.get('fromdate')
		truckreports = truckreports.filter(created_at__gte=fromdate)

	if request.GET.get('todate'):
		todate = request.GET.get('todate')
		truckreports = truckreports.filter(created_at__lte=todate)

	reporttypes = Reporttype.objects.filter(fortruck = True).order_by('reporttype').all()
	reportresolutions = Reportresolution.objects.order_by('resolution')
	truckreports = truckreports.order_by('created_at')[:100]

	context = { 'truckreports' : truckreports, 'reporttypes': reporttypes, 'reportresolutions': reportresolutions , 'clientname': clientname, 'complainttype': complainttype, 'complaintstatus': complaintstatus, 'truckname': truckname, 'fromdate': fromdate, 'todate': todate }
	return render(request, 'complaint/trucks.html', context)

@login_required
def index(request):
	reporttypes = Reporttype.objects
	reporttypes = reporttypes.order_by('reporttype')
	context = { 'reporttypes': reporttypes }
	return render(request, 'complaint/index.html', context)

@login_required
def updatecomplaint(request):
	id = int(request.POST.get('id'))
	forclient = bool(int(request.POST.get('forclient')))
	fortruck = bool(int(request.POST.get('fortruck')))
	active = bool(int(request.POST.get('active')))
	update = {
				'forclient': forclient, 
				'fortruck': fortruck, 
				'active': active
			}
	Reporttype.objects.filter(id = id).update(**update)
	return JsonResponse({'success': True})
