from django.shortcuts import render
from django.contrib.auth import authenticate, get_user_model, login, logout
from . forms import UserLoginForm #, UserRegisterForm
from django.shortcuts import redirect

def login_view(request):
	form = UserLoginForm(request.POST or None)
	if form.is_valid():
		username = form.cleaned_data.get("username")
		password = form.cleaned_data.get("password")
		user = authenticate(username= username, password= password)
		login(request, user)
		#return render(request, "clients/", {})
		return redirect('/client/')
	context = { "form": form }
	return render(request, "account/form.html", context)

#def register_view(request):
#	form = UserRegisterForm(request.POST or None)
#	context = {"form": form}
#	return render(request, "account/register.html", context)

def logout_view(request):
	logout(request)
	context = {}
	return redirect('/account/login')