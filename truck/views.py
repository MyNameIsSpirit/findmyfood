from django.shortcuts import render
from django.http import HttpResponse
from client.models import Truck, Menu, Clientreport, Truckreport, Truckmember, Cusinetype, Reportresolution, Menu, Foodtype, Truckfeedback, Truckreporthistory, AuthUser
from django.http import JsonResponse
from django.db.models import Q
import datetime
from django.shortcuts import redirect
from django.core import serializers
import json
from django.contrib.auth.decorators import login_required
import datetime

@login_required
def index(request):
	trucks = Truck.objects

	cusinetype = 0
	truckstatus = 0
	truckname = ''
	confirmed = 0
	fromdate= ''
	todate = ''

	if request.GET.get('truckname'):
		truckname = request.GET.get('truckname')
		if truckname is None:
			truckname = ''
		trucks = trucks.filter(Q(truckname__icontains=truckname))

	if request.GET.get('cusinetype'):
		cusinetype = int(request.GET.get('cusinetype'))
		trucks = trucks.filter(id_cusinetype=cusinetype)

	if request.GET.get('truckstatus'):
		truckstatus = request.GET.get('truckstatus')
		trucks = trucks.filter(active=bool(int(request.GET.get('truckstatus'))))

	if request.GET.get('confirmed'):
		confirmed = request.GET.get('confirmed')
		trucks = trucks.filter(confirmed=bool(int(request.GET.get('confirmed'))))

	if request.GET.get('fromdate'):
		fromdate = request.GET.get('fromdate')
		trucks = trucks.filter(created_at__gte=fromdate)

	if request.GET.get('todate'):
		todate = request.GET.get('todate')
		trucks = trucks.filter(created_at__lte=todate)

	trucks = trucks.order_by('truckname')[:100]
	cusinetypes = Cusinetype.objects.order_by('trucktype_en').all

	context = { 
				'trucks': trucks, 
				'cusinetypes': cusinetypes, 
				'cusinetype': cusinetype, 
				'truckname': truckname, 
				'cusinetype': cusinetype, 
				'truckstatus': truckstatus, 
				'confirmed': confirmed,
				'fromdate': fromdate,
				'todate': todate
			}
	return render(request, 'truck/index.html', context)

@login_required
def detail(request, id):
	fromdate = ''
	todate = ''
	truck = Truck.objects.get(id = id)
	
	clientreports = Clientreport.objects.filter(id_truck=id)
	truckreports = Truckreport.objects.filter(id_truck=id)
	truckfeedbacks = Truckfeedback.objects.filter(id_truck=id)

	if request.GET.get('fromdate'):
		fromdate = request.GET.get('fromdate')
		clientreports = clientreports.filter(created_at__gte=fromdate)
		truckreports = truckreports.filter(created_at__gte=fromdate)
		truckfeedbacks = truckfeedbacks.filter(created_at__gte=fromdate)

	if request.GET.get('todate'):
		todate = request.GET.get('todate')
		clientreports = clientreports.filter(created_at__lte=todate)
		truckreports = truckreports.filter(created_at__lte=todate)
		truckfeedbacks = truckfeedbacks.filter(created_at__lte=todate)

	clientreports = clientreports.order_by('created_at')[:100]
	truckreports = truckreports.order_by('created_at')[:100]
	truckfeedbacks = truckfeedbacks.order_by('created_at')[:100]

	menu = Menu.objects.filter(id_truck=id)
	truckmembers = Truckmember.objects.filter(id_truck=id)
	reportresolutions = Reportresolution.objects.order_by('resolution').all
	cusinetypes = Cusinetype.objects.order_by('trucktype_en').all
	foodtypes = Foodtype.objects.order_by('foodtype_en').all
	context = {'truck': truck, 'menu': menu, 'clientreports': clientreports, 'truckreports': truckreports, 'truckmembers': truckmembers, 'reportresolutions': reportresolutions, 'cusinetypes': cusinetypes, 'foodtypes': foodtypes, 'truckfeedbacks': truckfeedbacks , 'fromdate': fromdate, 'todate': todate }
	return render(request, 'truck/detail.html', context) 

@login_required
def truckComplaintResolution(request):
	id = int(request.POST.get('id'))
	idResolution = int(request.POST.get('resolution'))
	Truckreport.objects.filter(id = id).update(id_resolutiontype = idResolution)
	
	trh = Truckreporthistory()
	trh.id_truckreport = Truckreport(id=id)
	trh.id_reportresolution = Reportresolution(id=idResolution)
	trh.id_auth_user = AuthUser(id=request.user.id)
	trh.created_at = datetime.datetime.now()
	trh.save()
	return JsonResponse({'success': True})

@login_required
def updateTruck(request):
	id = request.POST.get("t_id")
	t_truckname = request.POST.get('t_truckname')
	t_businessemail = request.POST.get('t_businessemail')
	t_businessaddress = request.POST.get('t_businessaddress')
	t_phone = request.POST.get('t_phone')
	t_description_en = request.POST.get('t_description_en')
	t_description_es = request.POST.get('t_description_es')
	t_cusinetype = int(request.POST.get('cusinetype'))
	t_allday = bool(request.POST.get('allday'))
	
	update = {
				'truckname': t_truckname, 
				'businessemail': t_businessemail, 
				'businessaddress': t_businessaddress,
				'phone': t_phone,
				'description_es': t_description_es,
				'description_en': t_description_en,
				'id_cusinetype': t_cusinetype,
				'allday': t_allday
			}
	Truck.objects.filter(id = id).update(**update)
	return redirect('/truck/detail/' +  str(id))

@login_required
def blockManagement(request):
	id = int(request.POST.get('id'))
	active = int(request.POST.get('active'))
	Truck.objects.filter(id = id).update(active = active)
	return JsonResponse({'success': True})

@login_required
def menu(request):
	id = int(request.GET.get('id'))
	menuItem = Menu.objects.get(id=id)
	return JsonResponse({
							'success': True, 
							'title_en': menuItem.title_en,
							'title_es': menuItem.title_es, 
							'description_en': menuItem.description_en, 
							'description_es': menuItem.description_es,
							'price': menuItem.price,
							'active': menuItem.active,
							'id': menuItem.id,
							'foodtype_en': menuItem.id_foodtype.foodtype_en,
							'foodtype_es': menuItem.id_foodtype.foodtype_es,
							'id_foodtype': menuItem.id_foodtype.id,
							'picture': menuItem.picture,
							'specialprice': menuItem.specialprice,
						})

@login_required
def updateMenuItem(request):
	menuid = int(request.POST.get("menuid"))
	title_en = request.POST.get('title_en')
	description_en = request.POST.get('description_en')
	title_es = request.POST.get('title_es')
	description_es = request.POST.get('description_es')
	price = float(request.POST.get('price'))
	active = bool(request.POST.get('active'))
	id_foodtype = int(request.POST.get("id_foodtype"))
	specialprice = request.POST.get('specialprice')

	sp = 0
	if specialprice != "":
		sp = float(specialprice)
	
	update = {
				'title_en': title_en, 
				'description_en': description_en, 
				'title_es': title_es,
				'description_es': description_es,
				'price': price,
				'active': active,
				'id_foodtype': id_foodtype,
				'specialprice': sp
			}
	Menu.objects.filter(id = menuid).update(**update)
	return redirect('/truck/menu/?id=' +  str(menuid))
