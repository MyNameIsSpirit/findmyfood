from rest_framework import serializers
from client.models import Client, Cusinetype, Truck, Menu, Language

class ClientSerializer(serializers.ModelSerializer):
	class Meta:
		model = Client
		fields = ('name', 'lastname')

class PreferencesSerializer(serializers.ModelSerializer):
    selected = serializers.IntegerField()

    class Meta:
        model = Cusinetype
        fields = ('id', 'trucktype_en', 'trucktype_es', 'selected')

class TrucksSerializer(serializers.ModelSerializer):
	class Meta:
		model = Truck
		fields = ('id', 'truckname', 'tuckimage', 'trucklogo', 'description_en', 'description_es', 'allday', 'businessaddress', 'businessemail', 'phone')

class MenuSerializer(serializers.ModelSerializer):
	foodtype_en = serializers.CharField()
	#foodtype_es = serializers.CharField()
	class Meta:
		model = Menu
		fields = '__all__'#('id', 'title_en', 'title_es', 'price', 'description_en', 'description_es', 'active')

class CusineTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Cusinetype
        fields = ('id', 'trucktype_en', 'trucktype_es')

class LanguageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Language
        fields = ('id', 'language')