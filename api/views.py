from django.shortcuts import get_object_or_404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from client.models import Client, Language, Clienttype, Gender, Usersession, Device, Cusinetype, Foodpreference, Truckmember, Truck, Menu, Foodtype, Search
from .serializers import ClientSerializer, PreferencesSerializer, TrucksSerializer, MenuSerializer, CusineTypeSerializer, LanguageSerializer
import datetime
from django.utils.crypto import get_random_string
from django.db.models import Count, Case, When, IntegerField,F, Sum, CharField, Value 
from django.db.models.functions import Concat
from django.core import serializers
from django.conf import settings
from django.core.files.storage import FileSystemStorage
import requests
import json

class ClientList(APIView):
	def get(self, reqeuest):
		clients = Client.objects.all()
		serializers = ClientSerializer(clients, many = True)
		return Response(serializers.data)

class RegisterClient(APIView):
	def get(self, request):
		#permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
		id = request.GET.get('id')
		return Response({ "success" : True, 'id': id })
	def post(self, request):
		id = request.POST.get('id')
		return Response({ "success" : True, 'id': id, 'Method': 'POST' })


class LoginRegisterFB(APIView):
	def post(self, request):
		#me?fields=id,first_name,last_name,email,birthday,picture.width(240).height(240)
		fbid = int(request.POST.get('id'))
		firstname = request.POST.get('firstname')
		lastname = request.POST.get('lastname')
		email = request.POST.get('email')
		birthdate = request.POST.get('birthdate')
		picture = request.POST.get('picture')
		token = request.POST.get('token')
		deviceLanguage = request.POST.get('language')
		gender = request.POST.get('gender')
		latitude = float(request.POST.get('latitude'))
		longitude = float(request.POST.get('longitude'))
		iddevice = int(request.POST.get('idDevice'))
		uuid = request.POST.get('uuid')
		clientid = 0
		newuser = False

		#Check if email exist
		clientVerification = Client.objects.filter(email__iexact=email.lower())
		if clientVerification.count() == 0:
			#registerclient
			cl = Client()
			cl.name = firstname
			cl.lastname = lastname
			cl.picture = picture
			cl.id_clienttype = Clienttype(id=5)
			cl.active = True
			cl.created_at = datetime.datetime.now()
			cl.registeredLanguage = deviceLanguage
			cl.id_language = Language(id=1)
			cl.email = email
			cl.phone = ""
			cl.id_gender = Gender(id=2)
			cl.birthdate = birthdate
			cl.facebookid = fbid
			cl.save()
			clientid = cl.id
			newuser = True
			client = cl
		else:
			clientv = clientVerification.first() #Get the current client
			#Check is the client blocked?
			if clientv.active == False:
				return Response({ "success": False, "message": "Access denied!", "statuscode": 403 })
			clientid = clientv.id
			client = clientv

		#Close all the sessions for the same device
		sessions = Usersession.objects.filter(id_client=client.id, uuid=uuid, active=True).update(active=False)
		#print(sessions.query)

		#The client is available, then save the session
		sessionguid = get_random_string(length=64)
		us = Usersession()
		us.id_client = clientid
		us.latitude = latitude
		us.longitude = longitude
		us.active = True
		us.id_device = Device(id=iddevice)
		us.sessionguid = str(sessionguid)
		us.uuid = uuid
		us.created_at = datetime.datetime.now()
		us.fbtoken = token
		us.save()
		return Response({ "success": True, "message": "Access granted!", "session": us.sessionguid, "name": (client.name + " " + client.lastname), "photo": client.picture, "statuscode": 200, "newuser": newuser  })

class CusineTypes(APIView):
	def get(self, request):
		session = request.GET.get('session')
		
		usersessions = Usersession.objects.filter(sessionguid=session, active=True)
		print(usersessions.query)
		if usersessions.count() == 0:
			return Response({ "success": False, "message": "Access denied!", "statuscode": 403 })

		id_client = usersessions.first().id_client
		
		qs = Cusinetype.objects.filter().filter(active = True).annotate(
			selected=Count(Case(
		        When(foodpreference__id_client=id_client, then=1),
		        output_field=IntegerField()
		    ))

		)
		serializers = PreferencesSerializer(qs, many = True)
		return Response({ "success": True, "message": "Access granted!", "result": serializers.data, "statuscode": 200  })

class SuscribePreference(APIView):
	def post(self, request):
		session = request.POST.get('session')
		id_cusinetype = int(request.POST.get('idcusinetype'))

		usersessions = Usersession.objects.filter(sessionguid=session, active=True)
		if usersessions.count() == 0:
			return Response({ "success": False, "message": "Access denied!", "statuscode": 403 })

		id_client = usersessions.first().id_client
		fp = Foodpreference()
		fp.id_client = Client(id=id_client)
		fp.id_cusinetype = Cusinetype(id=id_cusinetype)
		fp.created_at = datetime.datetime.now()
		fp.save()
		return Response({ "success": True, "message": "Access granted!", "statuscode": 200  })

class UnsuscribePreference(APIView):
	def post(self, request):
		session = request.POST.get('session')
		id_cusinetype = int(request.POST.get('idcusinetype'))

		usersessions = Usersession.objects.filter(sessionguid=session, active=True)
		if usersessions.count() == 0:
			return Response({ "success": False, "message": "Access denied!", "statuscode": 403 })

		id_client = usersessions.first().id_client
		Foodpreference.objects.filter(id_client=id_client, id_cusinetype=id_cusinetype).delete()
		return Response({ "success": True, "message": "Access granted!", "statuscode": 200  })

class UpdatePhone(APIView):
	def post(self, request):
		session = request.POST.get('session')
		phone = request.POST.get('phone')

		usersessions = Usersession.objects.filter(sessionguid=session, active=True)
		if usersessions.count() == 0:
			return Response({ "success": False, "message": "Access denied!", "statuscode": 403 })

		id_client = usersessions.first().id_client
		Client.objects.filter(id = id_client).update(phone = phone)
		return Response({ "success": True, "message": "Access granted!", "statuscode": 200  })

class RegisterTruckGeneralInformation(APIView):
	def post(self, request):
		session = request.POST.get('session')
		usersessions = Usersession.objects.filter(sessionguid=session, active=True)
		if usersessions.count() == 0:
			return Response({ "success": False, "message": "Access denied!", "statuscode": 403 })

		id_client = usersessions.first().id_client

		truckname = request.POST.get('truckname')
		description_en = request.POST.get('description_en')
		description_es = request.POST.get('description_es')
		idcusinetype = int(request.POST.get('idcusinetype'))
		allday = bool(int(request.POST.get('allday')))

		truck = Truck()
		truck.truckname = truckname
		truck.active = True
		truck.description_en = description_en
		truck.description_es = description_es
		truck.id_cusinetype = Cusinetype(id=idcusinetype)
		truck.allday = allday
		truck.businessaddress = ''
		truck.businessemail = ''
		truck.phone = ''
		truck.confirmed = True
		truck.created_at = datetime.datetime.now()
		truck.save()

		truckmember = Truckmember()
		truckmember.id_client = Client(id=id_client)
		truckmember.id_truck = Truck(id=truck.id)
		truckmember.id_clienttype = Clienttype(id=5)
		truckmember.active = True
		truckmember.created_at = datetime.datetime.now()
		truckmember.deleted = False
		truckmember.save()

		#Elasticsearch
		truckdata = {
						'truckname': truck.truckname,
						'description_es': truck.description_es, 
						'description_en': truck.description_en,
						'allday': truck.allday,
						'active': True,
						'id': truck.id
				}
		r = requests.post("http://192.168.99.100:32769/fmf/truck/" + str(truck.id), data=json.dumps(truckdata))
		
		return Response({ "success": True, "message": "Access granted!", "statuscode": 200, "truckid": truck.id  })

class UpdateTruckContactInformation(APIView):
	def post(self, request):
		session = request.POST.get('session')
		idtruck = int(request.POST.get('idtruck'))
		businessemail = request.POST.get('businessemail')
		businessaddress = request.POST.get('businessaddress')
		phone = request.POST.get('phone')

		usersessions = Usersession.objects.filter(sessionguid=session, active=True)
		if usersessions.count() == 0:
			return Response({ "success": False, "message": "Access denied!", "statuscode": 403 })

		id_client = usersessions.first().id_client

		if Truckmember.objects.filter(id_client=id_client, id_truck=idtruck).count() == 0:
			return Response({ "success": False, "message": "Access denied!", "statuscode": 403 })

		update = {
				'businessemail': businessemail, 
				'businessaddress': businessaddress, 
				'phone': phone
			}
		Truck.objects.filter(id = idtruck).update(**update)

		return Response({ "success": True, "message": "Access granted!", "statuscode": 200  })

class UpdateTruckLogo(APIView):
	#backgroundimage, #logo
	def post(self, request):
		session = request.POST.get('session')
		idtruck = int(request.POST.get('idtruck'))

		usersessions = Usersession.objects.filter(sessionguid=session, active=True)
		if usersessions.count() == 0:
			return Response({ "success": False, "message": "Access denied!", "statuscode": 403 })

		id_client = usersessions.first().id_client

		if Truckmember.objects.filter(id_client=id_client, id_truck=idtruck).count() == 0:
			return Response({ "success": False, "message": "Access denied!", "statuscode": 403 })

		logofilename = "logo-" + get_random_string(length=64) + ".jpg"
		logofile = request.FILES['logo']
		fs = FileSystemStorage()
		filename = fs.save( logofilename , logofile)
		uploaded_file_url = fs.url(filename)

		update = { 
				'trucklogo': uploaded_file_url
			}
		Truck.objects.filter(id = idtruck).update(**update)
		return Response({ "success": True, "message": "Access granted!", "statuscode": 200 })

class UpdateTruckBanner(APIView):
	#backgroundimage, #logo
	def post(self, request):
		session = request.POST.get('session')
		idtruck = int(request.POST.get('idtruck'))

		usersessions = Usersession.objects.filter(sessionguid=session, active=True)
		if usersessions.count() == 0:
			return Response({ "success": False, "message": "Access denied!", "statuscode": 403 })

		id_client = usersessions.first().id_client

		if Truckmember.objects.filter(id_client=id_client, id_truck=idtruck).count() == 0:
			return Response({ "success": False, "message": "Access denied!", "statuscode": 403 })

		bannerfilename = "banner-" + get_random_string(length=64) + ".jpg"
		bannerfile = request.FILES['banner']

		fs = FileSystemStorage()
		bfilename = fs.save( bannerfilename , bannerfile)
		uploaded_file_url = fs.url(bfilename)

		update = {
				'tuckimage': bannerfilename
			}
		Truck.objects.filter(id = idtruck).update(**update)
		return Response({ "success": True, "message": "Access granted!", "statuscode": 200  })

class MyTrucks(APIView):
	def get(self, request):
		session = request.GET.get('session')

		usersessions = Usersession.objects.filter(sessionguid=session, active=True)
		if usersessions.count() == 0:
			return Response({ "success": False, "message": "Access denied!", "statuscode": 403 })

		id_client = usersessions.first().id_client

		trucks = Truck.objects.filter(truckmember__id_client=id_client)
		serializers = TrucksSerializer(trucks, many = True)
		return Response({ "success": True, "message": "Access granted!", "statuscode": 200, "trucks": serializers.data  })

class MenuByTruck(APIView):
	def get(self, request):
		idtruck = int(request.GET.get('idtruck'))

		menu = Menu.objects.filter(id_truck=idtruck).annotate(foodtype_en=Concat('id_foodtype__foodtype_en', Value(''), output_field=CharField())).prefetch_related('id_foodtype')
		print(menu.query)
		serializers = MenuSerializer(menu, many = True)

		return Response({ "success": True, "message": "Access granted!", "statuscode": 200, "menu": serializers.data })


class AddMenuItem(APIView):
	def post(self, request):
		session = request.POST.get('session')
		idtruck = int(request.POST.get('idtruck'))
		
		usersessions = Usersession.objects.filter(sessionguid=session, active=True)
		if usersessions.count() == 0:
			return Response({ "success": False, "message": "Access denied!", "statuscode": 403 })

		id_client = usersessions.first().id_client

		session = request.POST.get('session')
		#idtruck = int(request.POST.get('idtruck'))
		title_en = request.POST.get('titleen')
		title_es = request.POST.get('titlees')
		description_en = request.POST.get('descriptionen')
		description_es = request.POST.get('descriptiones')
		specialprice = request.POST.get('specialprice')
		price = float(request.POST.get('price'))
		id_foodtype = int(request.POST.get('idfoodtype'))

		fname = ''
		try:
			menufilename = "menu-" + get_random_string(length=64) + ".jpg"
			menufile = request.FILES['menu']
			fs = FileSystemStorage()
			filename = fs.save( menufilename , menufile)
			uploaded_file_url = fs.url(filename)
			fname = menufilename
		except Exception as e:
			print("")

		menu = Menu()
		menu.title_en = title_en
		menu.description_en = description_en
		menu.title_es = title_es
		menu.description_es = description_es
		menu.price = price
		menu.active = True
		menu.id_client = Client(id=id_client)
		menu.id_truck = Truck(id=idtruck)
		menu.id_foodtype = Foodtype(id=id_foodtype)
		menu.created_at = datetime.datetime.now()
		menu.picture = fname
		menu.save()

		#Elasticsearch
		menudata = {
						'title_es': title_es,
						'title_en': title_en, 
						'description_en': description_en,
						'description_es': description_es,
						'active': True,
						'id': menu.id
				}
		r = requests.post("http://192.168.99.100:32769/fmf/menu/" + str(menu.id), data=json.dumps(menudata))
		return Response({ "success": True, "message": "Access granted!", "statuscode": 200  })


class QuerySearch(APIView):
	def get(self, request):
		session = request.GET.get('session')
		searchedtext = request.GET.get('search')

		usersessions = Usersession.objects.filter(sessionguid=session, active=True)
		if usersessions.count() == 0:
			return Response({ "success": False, "message": "Access denied!", "statuscode": 403 })

		id_client = usersessions.first().id_client

		search = Search()
		search.id_usersession = usersessions.first()
		search.searchedtext = searchedtext
		search.created_at = datetime.datetime.now()
		search.save()

		#client = Client.objects.get(id=id_client)
		truckresults = []
		menuresults = []

		print("The searched text is: " + searchedtext)
		try:
			searchparams = {
							    "query": {
							        "multi_match" : {
							            "query" : searchedtext,
							            "fields": ["title_en", "description_en", "truckname"],
							            "fuzziness": "5"
							        }
							    },
							    "_source": ["title_en", "description_en", "active", "truckname", "id"],
							    "size": 25
							}
			
			headers = {'Content-Type': 'application/json; charset=UTF-8'}

			r = requests.post("http://192.168.99.100:32769/_search?pretty", data=json.dumps(searchparams), headers=headers)
			jsonresult = json.loads(r.text)
			
			elementsfound = int(jsonresult['hits']['total'])

			for i in range(0, elementsfound):
				if str(jsonresult['hits']['hits'][i]["_type"]) == "truck":
					truckresults.append(int(jsonresult['hits']['hits'][i]["_source"]["id"]))
				else:
					menuresults.append(int(jsonresult['hits']['hits'][i]['_source']['id']))

		except Exception as e:
			print("")

		trucks = Truck.objects.filter(id__in=truckresults)
		truckSerializers = TrucksSerializer(trucks, many = True)

		menutrucks = Truck.objects.filter(menu__id__in=menuresults)
		menuSerializers = TrucksSerializer(menutrucks, many = True)
		
		return Response({ "success": True, "message": "Access granted!", "statuscode": 200, 'trucks': truckSerializers.data, 'trucksbymenu': menuSerializers.data })

class Cusines(APIView):
	def get(self, request):
		cusinetypes = Cusinetype.objects.filter(active=True).order_by('trucktype_en')
		ctSerializers = CusineTypeSerializer(cusinetypes, many = True)
		return Response({ "success": True, "message": "Access granted!", "statuscode": 200, 'cusinetypes': ctSerializers.data })

class UpdateTruckGeneralInformation(APIView):
	def post(self, request):
		session = request.POST.get('session')
		usersessions = Usersession.objects.filter(sessionguid=session, active=True)
		if usersessions.count() == 0:
			return Response({ "success": False, "message": "Access denied!", "statuscode": 403 })

		id_client = usersessions.first().id_client

		truckname = request.POST.get('truckname')
		description_en = request.POST.get('description_en')
		description_es = request.POST.get('description_es')
		idcusinetype = int(request.POST.get('idcusinetype'))
		allday = bool(int(request.POST.get('allday')))
		idtruck = int(request.POST.get('idtruck'))

		update = {
				'truckname': truckname, 
				'description_en': description_en, 
				'description_es': description_es,
				'id_cusinetype': Cusinetype(id=idcusinetype),
				"allday": allday
			}
		Truck.objects.filter(id = idtruck).update(**update)
		return Response({ "success": True, "message": "Access granted!", "statuscode": 200 })

class UpdatePhone(APIView):
	def post(self, request):
		session = request.POST.get('session')
		phone = request.POST.get('phone')
		usersessions = Usersession.objects.filter(sessionguid=session, active=True)
		if usersessions.count() == 0:
			return Response({ "success": False, "message": "Access denied!", "statuscode": 403 })

		id_client = usersessions.first().id_client
		Client.objects.filter(id=id_client).update(phone=phone)
		return Response({ "success": True, "message": "Access granted!", "statuscode": 200 })

class Languages(APIView):
	def get(self, request):
		language = Language.objects.order_by('language')
		languageSerializers = LanguageSerializer(language, many = True)
		return Response({ "success": True, "message": "Access granted!", "statuscode": 200, 'languages': languageSerializers.data })

class UpdateLanguage(APIView):
	def post(self, request):
		session = request.POST.get('session')
		idlanguage = request.POST.get('idlanguage')
		usersessions = Usersession.objects.filter(sessionguid=session, active=True)
		if usersessions.count() == 0:
			return Response({ "success": False, "message": "Access denied!", "statuscode": 403 })

		id_client = usersessions.first().id_client
		Client.objects.filter(id=id_client).update(id_language=Language(id=idlanguage) )
		return Response({ "success": True, "message": "Access granted!", "statuscode": 200 })

class UpdateMenuItem(APIView):
	def post(self, request):
		session = request.POST.get('session')
		title_en = request.POST.get('titleen')
		title_es = request.POST.get('titlees')
		description_en = request.POST.get('descriptionen')
		description_es = request.POST.get('descriptiones')
		price = float(request.POST.get('price'))
		specialprice = float(request.POST.get('specialprice'))
		active = bool(request.POST.get('active'))
		id_foodtype = int(request.POST.get('idfoodtype'))
		id_menu = int(request.POST.get('id'))
		
		usersessions = Usersession.objects.filter(sessionguid=session, active=True)
		if usersessions.count() == 0:
			return Response({ "success": False, "message": "Access denied!", "statuscode": 403 })

		menu = Menu.objects.get(id=id_menu)
		
		id_truck = menu.id_truck.id
		id_client = usersessions.first().id_client

		#tm = Truckmember.objects.filter(id_client = id_client, id_truck = id_truck, deleted = False, active = True, id_clienttype = 5 )
		tm = Truckmember.objects.filter(id_client = id_client, id_truck = id_truck, deleted = False, active = True )
		if tm.count() == 0:
			return Response({ "success": False, "message": "Access denied!", "statuscode": 403 })

		update = {
				'title_en': title_en, 
				'title_es': title_es, 
				'description_es': description_es,
				'description_en': description_en,
				"price": price,
				"specialprice": specialprice,
				"active": active,
				"id_foodtype": Foodtype(id=id_foodtype)
			}
		Menu.objects.filter(id = id_menu).update(**update)
		
		return Response({ "success": True, "message": "Access granted!", "statuscode": 200 })

class UpdateMenuPhoto(APIView):
	def post(self, request):
		session = request.POST.get('session')
		id_menu = request.POST.get('id')

		usersessions = Usersession.objects.filter(sessionguid=session, active=True)
		if usersessions.count() == 0:
			return Response({ "success": False, "message": "Access denied!", "statuscode": 403 })

		id_client = usersessions.first().id_client


		idtruck = Menu.objects.get(id = id_menu).id_truck
		print("The truck id is: " + str(idtruck))


		if Truckmember.objects.filter(id_client=id_client, id_truck=idtruck).count() == 0:
			return Response({ "success": False, "message": "Access denied!", "statuscode": 403 })

		menufilename = "menu-" + get_random_string(length=64) + ".jpg"
		logofile = request.FILES['menu']
		fs = FileSystemStorage()
		filename = fs.save( menufilename , logofile)
		uploaded_file_url = fs.url(filename)

		update = { 
				'picture': uploaded_file_url
			}
		Menu.objects.filter(id = id_menu).update(**update)
		return Response({ "success": True, "message": "Access granted!", "statuscode": 200 })
#send an invite
#confirm invite

#update photos
