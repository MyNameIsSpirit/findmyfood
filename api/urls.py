"""findmyfood URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from . import views
from rest_framework.urlpatterns import format_suffix_patterns

app_name = 'api'

urlpatterns = [
    url(r'^clients/', views.ClientList.as_view()),
    #url(r'^fblogin/', views.RegisterClient.as_view()),
    url(r'^fblogin/', views.LoginRegisterFB.as_view()),
    url(r'^preferences/', views.CusineTypes.as_view()),
    url(r'^suscribepreference/', views.SuscribePreference.as_view()),
    url(r'^unsuscribepreference/', views.UnsuscribePreference.as_view()),
    url(r'^updatephone/', views.UpdatePhone.as_view()),
    url(r'^registerTruckGeneralInformation/', views.RegisterTruckGeneralInformation.as_view()),
    url(r'^updateTruckGeneralInformation/', views.UpdateTruckGeneralInformation.as_view()),
    url(r'^updateTruckContactInformation/', views.UpdateTruckContactInformation.as_view()),
    url(r'^updateTruckLogo/', views.UpdateTruckLogo.as_view()),
    url(r'^updateTruckBanner/', views.UpdateTruckBanner.as_view()),
    url(r'^myTrucks/', views.MyTrucks.as_view()),
    url(r'^menu/', views.MenuByTruck.as_view()),
    url(r'^addmenuitem/', views.AddMenuItem.as_view()),
    url(r'^updatemenuitem/', views.UpdateMenuItem.as_view()),
    url(r'^updatemenuphoto/', views.UpdateMenuPhoto.as_view()),
    url(r'^search/', views.QuerySearch.as_view()),
    url(r'^cusinetypes/', views.Cusines.as_view()),
    url(r'^updatePhone/', views.UpdatePhone.as_view()),
    url(r'^languages/', views.Languages.as_view()),
    url(r'^updatelanguage/', views.UpdateLanguage.as_view()),
    #url(r'^updatemenu/', views.UpdateMenu.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)
