from django.shortcuts import render
from django.http import HttpResponse
from client.models import Foodtype, Cusinetype
from django.contrib.auth.decorators import login_required

@login_required
def food(request):
	foodtypes = Foodtype.objects.all().order_by('foodtype_en')
	context = { 'foodtypes': foodtypes }
	return render(request, 'catalog/food.html', context)

@login_required
def cusine(request):
	cusinetypes = Cusinetype.objects.all().order_by('trucktype_en')
	context = { 'cusinetypes': cusinetypes }
	return render(request, 'catalog/cusine.html', context)

